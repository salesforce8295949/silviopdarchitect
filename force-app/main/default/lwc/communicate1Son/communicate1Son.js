import { LightningElement, api } from "lwc";

export default class Communicate1Son extends LightningElement {
  @api son;
  @api son2;
  @api sonvisible1 = false;
  @api sonvisible2 = false;
}