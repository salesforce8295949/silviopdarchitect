import { LightningElement, api } from "lwc";

export default class Communication2Son extends LightningElement {
  @api visiblechild1 = false;
  @api visiblechild2 = false;

  handleChange = (event) => {
    this.dispatchEvent(
      new CustomEvent("handlechange", { detail: event.target.value })
    );
  };

  handleClick = () => {
    let sonValue = this.template.querySelector('[data-id="inputSon"]');
    console.log("value click", sonValue.value);
    this.dispatchEvent(
      new CustomEvent("handlechange2", { detail: sonValue.value })
    );
  };
}