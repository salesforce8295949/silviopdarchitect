import { LightningElement } from "lwc";

export default class Communicate1Father extends LightningElement {
  father = "";
  father2 = "";

  handleChange = (event) => {
    this.father = event.target.value;
  };

  handleClick = () => {
    console.log("click");
    let fatherValue = this.template.querySelector('[data-id="inputFather"]');
    console.log(fatherValue.value);
    this.father2 = fatherValue.value;
  };
}