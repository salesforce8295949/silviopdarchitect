import { LightningElement } from "lwc";

export default class Communication2Father extends LightningElement {
  son = "";
  son2 = "";

  handleSon1(e) {
    this.son = e.detail;
  }

  handleSon2(e) {
    this.son2 = e.detail;
  }
}