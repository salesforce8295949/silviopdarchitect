public with sharing class ContactController {
  @AuraEnabled(cacheable=true)
  public static List<Contact> getContacts() {
    try {
      return [SELECT Name, Email, Phone FROM Contact];
    } catch (Exception e) {
      System.debug(e.getMessage());
      return new List<Contact>();
    }
  }
}